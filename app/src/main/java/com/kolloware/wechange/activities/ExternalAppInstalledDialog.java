package com.kolloware.wechange.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.kolloware.wechange.BaseApplication;
import com.kolloware.wechange.R;
import com.kolloware.wechange.models.ExternalAppInformation;

public class ExternalAppInstalledDialog extends AppCompatDialogFragment {

    private ExternalAppInformation appInfo;

    public ExternalAppInstalledDialog(ExternalAppInformation inAppInfo) {
        this.appInfo = inAppInfo;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String appTitle = getString(appInfo.appTitleResourceKey);

        String message = getString(R.string.dialog_external_app_installed, appTitle);
        if (appInfo.instructionsResourceKey != null) {
            message += "\n\n" + getString(appInfo.instructionsResourceKey);
        }
        if (appInfo.canRunInBrowser()) {
            message += "\n\n" + getString(R.string.dialog_external_app_browser_alternative);
        }

        builder = builder.setTitle(R.string.dialog_external_app_title)
                .setMessage(message)
                .setPositiveButton(getString(R.string.dialog_external_app_open_app, appTitle)
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                BaseApplication.openApp(getContext(), appInfo.appPackage);
                            }
                        })
                .setNegativeButton(R.string.dialog_external_app_cancel, null)
                .setNeutralButton(R.string.dialog_external_app_open_in_browser,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getContext().startActivity(new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(appInfo.browserURL)));
                            }
                        });

        return builder.create();
    }
}
