package com.kolloware.wechange.activities;

import android.Manifest;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.core.app.ActivityCompat;

import com.kolloware.wechange.BaseApplication;
import com.kolloware.wechange.Constants;
import com.kolloware.wechange.models.ExternalAppInformation;
import com.kolloware.wechange.utils.MarkNotificationsSeenTask;
import com.kolloware.wechange.R;
import com.kolloware.wechange.models.ViewModel;
import com.kolloware.wechange.utils.WechangeCookie;

import org.jsoup.Connection;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements Constants {

    private WebView browser;
    private ProgressBar progressBar;

    private ActionMenuItemView actionBack;
    private ActionMenuItemView actionForward;

    private Map<String, ExternalAppInformation> appsToLaunchByURL;

    public MainActivity() {
        appsToLaunchByURL = new HashMap<>();
        appsToLaunchByURL.put(WECHANGE_MESSAGES_URL,
                new ExternalAppInformation(APP_ROCKET_CHAT,
                        R.string.app_title_rocket_chat,
                        R.string.app_instructions_rocket_chat,
                        WECHANGE_ROCKET_CHAT_URL));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Log.d(LOG_UI, getClass().getSimpleName()
                + ".onNewIntent() called with: intent = [" + intent + "]");

        Bundle intentExtras = intent.getExtras();
        Double newestTimestamp = null;

        if (intentExtras != null) {
            String urlFromIntent = intentExtras.getString(INTENT_KEY_URL);
            Log.v(LOG_APP, "url from intent: " + urlFromIntent);

            // disallow file:// and javascript: URLs
            if ((urlFromIntent != null) && (
                    urlFromIntent.trim().startsWith("javascript") ||
                            (urlFromIntent.trim().startsWith("file")))) {
                Log.e(LOG_APP, "invalid url, do not load");
            }
            else {
                // deactivate to avoid cross-app-scripting
                // ViewModel.currentURL = urlFromIntent;
            }

            newestTimestamp = intentExtras.getDouble(INTENT_KEY_TIMESTAMP);
            (new MarkNotificationsSeenTask(newestTimestamp)).execute();
        }

        // deactivate to avoid cross-app-scripting
        // browser.loadUrl(ViewModel.currentURL);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreate() called with: savedInstanceState = ["
                + savedInstanceState + "]");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progressbar);

        browser = findViewById(R.id.webview);
        browser.getSettings().setLoadsImagesAutomatically(true);
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setAllowFileAccess(false);
        browser.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        browser.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.v(LOG_NET, "URL requested: " + url);

                for (Map.Entry<String, ExternalAppInformation> forEntry : appsToLaunchByURL.entrySet()) {
                    if (url.startsWith(forEntry.getKey())) {
                        Log.v(LOG_NET, "URL found. Starting external app");
                        launchExternalApp(forEntry.getValue());
                        return true;
                    }
                }

                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showProgressBar(true);
                ViewModel.currentURL = url;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                showProgressBar(false);
                updateNavigationVisibilities();
            }
        });

        browser.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url,
                                        String userAgent,
                                        String contentDisposition,
                                        String mimetype,
                                        long contentLength) {
                downloadFileToExternalStorage(url, userAgent, contentDisposition, mimetype, contentLength);
            }
        });

        browser.loadUrl(ViewModel.currentURL);

        String cookieString = CookieManager.getInstance().getCookie(ViewModel.currentURL);

        if (cookieString != null && !cookieString.isEmpty()) {
            ViewModel.cookie = new WechangeCookie(CookieManager.getInstance().getCookie(ViewModel.currentURL));
            Log.i(LOG_NET, "Cookie: " + ViewModel.cookie);
        }
        else {
            ViewModel.cookie = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    private int getVisibilityByBoolean(boolean visible) {
        return visible ? View.VISIBLE : View.INVISIBLE;
    }

    private void updateNavigationVisibilities() {
        if (actionBack == null) { actionBack = findViewById(R.id.action_back); }
        if (actionForward == null) { actionForward = findViewById(R.id.action_forward); }

        if (actionBack == null) return;

        actionBack.setVisibility(getVisibilityByBoolean(browser.canGoBack()));
        actionForward.setVisibility(getVisibilityByBoolean(browser.canGoForward()));
    }

    private void showProgressBar(boolean visible) {
        if (visible) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setIndeterminate(true);
        }
        else {
            progressBar.setVisibility(View.GONE);
            updateNavigationVisibilities();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_back:
                if (browser.canGoBack()) {
                    browser.goBack();
                }
                break;
            case R.id.action_forward:
                if (browser.canGoForward()) {
                    browser.goForward();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && browser.canGoBack()) {
            browser.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void launchExternalApp(ExternalAppInformation appInfo) {
        if (BaseApplication.isAppInstalled(getApplicationContext(),
                appInfo.appPackage)) {
            ExternalAppInstalledDialog dialog = new ExternalAppInstalledDialog(appInfo);
            dialog.show(getSupportFragmentManager(),  TAG_EXTERNAL_APP_INSTALLED);
        }
        else {
            ExternalAppNotInstalledDialog dialog = new ExternalAppNotInstalledDialog(appInfo);
            dialog.show(getSupportFragmentManager(),  TAG_EXTERNAL_APP_NOT_INSTALLED);
        }
    }

    private boolean storagePermissionGiven() {
        final int REQUEST_EXTERNAL_STORAGE = 1;
        final String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );

            return false;
        }

        return true;
    }

    private void downloadFileToExternalStorage(String url,
                                               String userAgent,
                                               String contentDisposition,
                                               String mimetype,
                                               long contentLength) {
        if (storagePermissionGiven()) {
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

            String cookie = CookieManager.getInstance().getCookie(url);
            request.addRequestHeader("Cookie", cookie);
            request.addRequestHeader("User-Agent", userAgent);
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(
                    DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

            DownloadManager downloadmanager = (DownloadManager) getSystemService(
                    Context.DOWNLOAD_SERVICE);

            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                    URLUtil.guessFileName(url, contentDisposition, mimetype));

            downloadmanager.enqueue(request);
        }
    }
}