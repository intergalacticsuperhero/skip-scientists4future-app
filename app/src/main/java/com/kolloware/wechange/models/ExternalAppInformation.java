package com.kolloware.wechange.models;

public class ExternalAppInformation {
    public final String appPackage;
    public final Integer appTitleResourceKey;
    public final Integer instructionsResourceKey;
    public final String browserURL;

    public ExternalAppInformation(String inAppPackage,
                                  Integer inAppTitleResourceKey,
                                  Integer inInstructionsResourceKey,
                                  String inBrowserURL) {
        this.appPackage = inAppPackage;
        this.appTitleResourceKey = inAppTitleResourceKey;
        this.instructionsResourceKey = inInstructionsResourceKey;
        this.browserURL = inBrowserURL;
    }

    public boolean canRunInBrowser() {
        return (browserURL != null);
    }
}
