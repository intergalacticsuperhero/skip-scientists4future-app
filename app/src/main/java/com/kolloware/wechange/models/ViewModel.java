package com.kolloware.wechange.models;

import com.kolloware.wechange.utils.WechangeCookie;

import java.util.HashSet;
import java.util.Set;

import static com.kolloware.wechange.Constants.WECHANGE_URL;

public class ViewModel {

    public static String currentURL = WECHANGE_URL;
    public static WechangeCookie cookie = null;
    public static Set<Integer> notifiedIds = new HashSet<>();

    private ViewModel() {
    }
}
