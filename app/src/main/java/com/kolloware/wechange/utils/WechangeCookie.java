package com.kolloware.wechange.utils;

import java.util.HashMap;
import java.util.Map;

public class WechangeCookie {

    private final static String KEY_CSRF_TOKEN = "csrftoken";
    private final static String DELIMITER_SEMICOLON = "; ";
    private final static String DELIMITER_EQUALS = "=";

    private Map<String, String> map = new HashMap<>();
    private String cookieString;

    public WechangeCookie(String inCookie) {
        cookieString = inCookie;

        for (String forKeyValuePair : inCookie.split(DELIMITER_SEMICOLON)) {
            String[] forCookie = forKeyValuePair.split(DELIMITER_EQUALS);
            map.put(forCookie[0], forCookie[1]);
        }
    }

    public String toString() {
        return cookieString;
    }

    public String getCsrfToken() { return map.get(KEY_CSRF_TOKEN); }
}
